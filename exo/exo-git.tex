\setupcolors[state=start]       % otherwise you get greyscale
\definecolor[headingcolor][r=0,g=0,b=0]

% for the document info/catalog (reported by 'pdfinfo', for example)
\setupinteraction[state=start,  % make hyperlinks active, etc.
  title={Exercices Git},
  author={Millian Poquet},
]

\useURL[author-email][mailto:millian.poquet@inria.fr][][millian.poquet@inria.fr]

\setuplayout[topspace=0.5in, backspace=1in, header=24pt, footer=36pt,
  height=middle, width=middle]

% headers and footers
\setupfooter[style=\it]
\setupfootertexts[]
\setuppagenumbering[location={footer,middle}, style=bold]

\definecolumnset[twocolumns][n=2]

\setupbodyfont[11pt]            % default is 12pt

\setuphead[section,chapter,subject][color=headingcolor]
\setuphead[section,subject][style={\ss\bfa},
  before={\bigskip\bigskip}, after={}]
\setuphead[chapter][style={\ss\bfd}]
\setuphead[title][style={\ss\bfd},
  before={\begingroup\setupbodyfont[14.4pt]},
  after={\leftline{\ss\tfa Millian Poquet (\from[author-email])}
         \bigskip\bigskip\endgroup}]

\setupitemize[inbetween={}, style=bold]

% set inter-paragraph spacing
\setupwhitespace[medium]
% comment the next line to not indent paragraphs
\setupindenting[medium, yes]

\starttext

\title{Git : Incorporation de branches}

\useurl[devops_framagit_group][https://framagit.org/devops-course][][groupe devops-course de Framagit]

Le but de ce TP est de manipuler les principales manières d'incorporer des modifications dans un projet git.
Les différentes situations présentées existent dans des dépôts git du \from[devops_framagit_group],
ce qui vous permet de récupérer les dépôts pour manipuler les commandes git.
Chaque situation est présentée par deux figures :
la situation initiale à gauche, et la situation souhaitée à droite.

\section{Rappel de commandes pratiques}
\startitemize
\item \type{git status} montre diverses informations sur votre répertoire de travail. \\
      Elle fournit de précieux conseils dans de nombreuses situations, n'hésitez pas à vous en servir dès que vous rencontrez une situation inattendue (comme lors d'une résolution de conflit, en cas \type{rebase} interactif ou de recherche de bug dichotomique via \type{git bisect}).
\item \type{git log} permet de visualiser la forêt de commits. \\
      En particulier, \type{git log --all --graph --oneline} montre toutes les branches (\type{--all}),
      affiche les liens de parenté entre les commits (\type{--graph}) de manière concise (\type{--oneline}).
\item \type{git checkout BRANCH} permet de se déplacer dans la branch \type{BRANCH}. \\
      Autrement dit, cette commande fait pointer \type{HEAD} vers \type{BRANCH}.
\item \type{git branch -f BRANCH} fait pointer \type{BRANCH} vers votre situation courante. \\
      Autrement dit, cette commande fait pointer \type{BRANCH} vers le commit sur lequel \type{HEAD} pointe.
\item \type{git merge BRANCH} fusionne la branche \type{BRANCH} dans votre situation courante. \\
      Cette commande peut ou non créer un commit de fusion selon votre configuration et les branches à fusionner.
      Les options \type{--ff}, \type{--ff-only} et \type{--no-ff} de la commande permettent de contrôler finement la création ou non de commit de fusion.
\item \type{git rebase BRANCH} permet de déplacer vos modifications courantes \italic{au-dessus} de \type{BRANCH}. \\
      C'est une commande extrêmement pratique pour garder un dépôt git lisible, mais elle peut être dangereuse car elle réécrit l'historique.
      Il est souvent déconseillé de s'en servir lorsque les modifications sons publiques, c'est-à-dire non locales à votre dépôt personnel et potentiellement utilisées par d'autres personnes que vous.
\stopitemize

\page
\section{Situations}
\subject{Incorporation simple (fast forward)}
Ce cas est le plus simple.
Il arrive lorsqu'on souhaite intégrer les modifications d'un développeur qui a travaillé juste \italic{au-dessus} de la branche que l'on souhaite mettre à jour.
Accès au dépôt :
\startitemize
\item \type{git clone git@framagit.org:devops-course/exo-git-merge-ff.git} (git)
\item \type{git clone https://framagit.org/devops-course/exo-git-merge-ff.git} (https)
\stopitemize

\startcolumnset[twocolumns]
\placefigure[right,none]{}{\externalfigure[fig/exo0-init.pdf][height=8.2cm]}
\column
\placefigure[right,none]{}{\externalfigure[fig/exo0-merge-ff.pdf][height=8.2cm]}
\stopcolumnset


\subject{Incorporation simple (merge commit)}
C'est le même cas que le précédent, mais on souhaite ici conserver dans l'historique la fusion de \type{green} dans \type{blue} grâce à un commit de fusion.
Accès au dépôt :
\startitemize
\item \type{git clone git@framagit.org:devops-course/exo-git-merge-ff.git} (git)
\item \type{git clone https://framagit.org/devops-course/exo-git-merge-ff.git} (https)
\stopitemize

\startcolumnset[twocolumns]
\placefigure[right,none]{}{\externalfigure[fig/exo0-init.pdf][height=8cm]}
\column
\placefigure[right,none]{}{\externalfigure[fig/exo0-merge-no-ff.pdf][height=9cm]}
\stopcolumnset


\subject{Divergence (double merge)}
Ce cas est extrêmement courant. Il arrive dès que vos modifications locales (\type{green}) ont été faites à partir d'une branche de base (\type{blue}), mais que la branche a continué à évoluer (\type{green2}) sur le dépôt distant le temps que vous fassiez vos modifications.
Pour les besoins de ce TP, \type{blue} et \type{blue2} sont deux branches différentes, mais imaginez qu'il s'agit de la même branche.

Une manière sure d'incorporer vos modifications est de procéder en deux étapes.
D'abord vérifier que vos modifications marchent toujours à partir de la version à jour de la branche de base,
ce qui peut se faire en fusionnant \type{blue2} dans \type{green} (en situation réelle, on lançerait alors les tests à ce moment-là).
Ensuite, intégrer votre nouveau \type{green} dans \type{blue}.
Accès au dépôt :
\startitemize
\item \type{git clone git@framagit.org:devops-course/exo-git-divergence.git} (git)
\item \type{git clone https://framagit.org/devops-course/exo-git-divergence.git} (https)
\stopitemize

\startcolumnset[twocolumns]
\placefigure[right,none]{}{\externalfigure[fig/exo1-init.pdf][height=5.5cm]}
\column
\placefigure[right,none]{}{\externalfigure[fig/exo1-merge-merge.pdf][height=8cm]}
\stopcolumnset


\subject{Divergence (simple merge)}
Ce cas est le même que le précédent, sauf que l'on souhaite ici directement fusionner la branche \type{green} dans la base à jour \type{blue2}.
C'est en général une manière de faire à éviter pour des modifications importantes (par exemple modification du code), mais elle est pertinente pour des modifications indépendantes et non critique du type mise à jour de documentation.
Accès au dépôt :
\startitemize
\item \type{git clone git@framagit.org:devops-course/exo-git-divergence.git} (git)
\item \type{git clone https://framagit.org/devops-course/exo-git-divergence.git} (https)
\stopitemize

\startcolumnset[twocolumns]
\placefigure[right,none]{}{\externalfigure[fig/exo1-init.pdf][height=7cm]}
\column
\placefigure[right,none]{}{\externalfigure[fig/exo1-merge2.pdf][height=9cm]}
\stopcolumnset

\subject{Divergence (rebase)}
Ce cas est le même que les deux précédents.
Il montre que l'on peut \italic{déplacer} les commits de \type{green} \italic{au-dessus} de \type{blue2} grâce à \type{git rebase}.
Cela permet de se placer dans un cas de fusion où la version réécrite de \type{green} peut être directement fusionnée dans \type{blue2} (fast forward), comme vu en tout début de TP.
Accès au dépôt :
\startitemize
\item \type{git clone git@framagit.org:devops-course/exo-git-divergence.git} (git)
\item \type{git clone https://framagit.org/devops-course/exo-git-divergence.git} (https)
\stopitemize

\startcolumnset[twocolumns]
\placefigure[right,none]{}{\externalfigure[fig/exo1-init.pdf][height=7cm]}
\column
\placefigure[right,none]{}{\externalfigure[fig/exo1-rebase.pdf][height=11cm]}
\stopcolumnset

\subject{Divergence et conflits}
Ce cas est le relativement courant lorsque nos modifications rentrent en conflit avec celles réalisées dans la version à jour du dépôt.
On veut ici incorporer nos modifications conflictuelles (\type{orange}) dans \type{blue2} par une fusion simple.
Utilisez \type{git status} pour voir quelles commandes permettent de résoudre le conflit --- et résolvez-le.
Accès au dépôt :
\startitemize
\item \type{git clone git@framagit.org:devops-course/exo-git-conflict.git} (git)
\item \type{git clone https://framagit.org/devops-course/exo-git-conflict.git} (https)
\stopitemize

\startcolumnset[twocolumns]
\placefigure[right,none]{}{\externalfigure[fig/exo2-init.pdf][height=7cm]}
\column
\placefigure[right,none]{}{\externalfigure[fig/exo2-merge2.pdf][height=9cm]}
\stopcolumnset

\stoptext
